const path = require('path');
const webpack = require('webpack');
module.exports = {
  entry: './index.js',
  devtool: 'source-map',
  output: {
    path: path.resolve(__dirname, './build'),
    filename: 'bundle.js',
    publicPath: '/build/'
  },
  eslint: {
    configFile: path.resolve('.eslintrc')
  },
  resolveLoader: {
    root: path.join(__dirname, 'node_modules')
  },
  devServer: {
    port: process.env.PORT || 7070,
    host: '0.0.0.0',
    historyApiFallback: true
  },
  module: {
    preLoaders: [
      {test: /\.js$/, loader: 'eslint-loader', exclude: /node_modules/ }
    ],
    loaders: [
      {
        test: /\.s?css$/,
        loaders: ['style-loader', 'css-loader', 'sass-loader']
      },
      {test: /\.js$/, loader: 'babel'},
      {test: /\.html$/, loader: 'babel'},
      {test: /\.html$/, loader: 'wc-loader?minify=true'},
      {test: /\.json$/, loader: 'json'},
      {
        test: /\.(png|jpg|gif)$/,
        loader: 'file',
        query: {
          name: 'images/[name].[ext]?[hash]'
        }
      },
      {
        test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "url"
      },
      {
        test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
        loader: 'file',
        query: {
          name: 'fonts/[name].[ext]?[hash]'
        }
      }
    ],
    postLoaders: [
      {
        test: [/\.js$/],
        exclude: /(node_modules|bower_components)/,
        loader: 'documentation'
      }
    ]
  },
  documentation: {
    entry: './components/**/**/*.js',
    github: true,
    shallow: true,
    format: 'html',
    output: './docs'
  }
};
