/* Bower Components */
import './bower_components/polymer/polymer.html';

/* Font-awesome */
import './bower_components/font-awesome/css/font-awesome.min.css';

/* co-dls */
import './bower_components/co-dls/build/co-dls.css';

/* Components */
import './components/co-alert/co-alert.html';
import './components/co-button/co-button.html';
import './components/co-calendar/co-calendar.html';
import './components/co-chart/co-chart.html';
import './components/co-checkbox/co-checkbox.html';
import './components/co-clip-board/co-clip-board.html';
import './components/co-currency/co-currency.html';
import './components/co-data-table/co-data-table.html';
import './components/co-date/co-date.html';
import './components/co-date-picker/co-date-picker.html';
import './components/co-drop-down/co-drop-down.html';
import './components/co-editor/co-editor.html';
import './components/co-file-upload/co-file-upload.html';
import './components/co-list-view/co-list-view.html';
import './components/co-modal/co-modal.html';
import './components/co-nav/co-nav.html';
import './components/co-notification/co-notification.html';
import './components/co-pagination/co-pagination.html';
import './components/co-panel/co-panel.html';
import './components/co-radio-group/co-radio-group.html';
import './components/co-slider/co-slider.html';
import './components/co-spinner/co-spinner.html';
import './components/co-tabs/co-tabs.html';
import './components/co-text-box/co-text-box.html';
import './components/co-toggle/co-toggle.html';
import './components/co-tooltip/co-tooltip.html';


/* Fixtures */
import './fixtures/modal-example.html';

/* Container Element */
import './components/container.html';
